package com.sda;

import java.util.Scanner;

public class MainTablicaMechanik {
    public static void main(String[] args) {
        Boolean[] miejscaParkingowe = new Boolean[5];

        for (int i = 0; i < miejscaParkingowe.length; i++) {
            miejscaParkingowe[i] = false;
        }

        // 0 , 1, 2, 3, 4

        // dodaj
        // sprawdz
        // wypisz (wszystkie miejsca)
        Scanner scanner = new Scanner(System.in);
        System.out.println();

        String komenda;
        do { // wykona się co najmniej raz
            System.out.println("Podaj komendę: (dodaj/sprawdz/wypisz)");
            komenda = scanner.next();

            if (komenda.equals("dodaj")) {
                System.out.println("Podaj numer miejsca:");
                int numerMiejsca = scanner.nextInt();

                if (numerMiejsca >= 0 && numerMiejsca < 5) {
                    // numer miejsca jest poprawny
                    if (!miejscaParkingowe[numerMiejsca]) { // nie 'miejsce parkingowe jest zajęte'
//                    if (miejscaParkingowe[numerMiejsca] == false) { // nie 'miejsce parkingowe jest zajęte'
                        miejscaParkingowe[numerMiejsca] = true;
                        System.out.println("Miejsce zostało zajęte.");
                    } else {
                        System.out.println("Miejsce było zajęte!");
                    }
                } else {
                    System.out.println("Niepoprawny numer miejsca!");
                }
            } else if (komenda.equals("sprawdz")) {
                System.out.println("Podaj numer miejsca:");
                int numerMiejsca = scanner.nextInt();
                System.out.println("Miejsce numer " + numerMiejsca + " jest zajęte: " + miejscaParkingowe[numerMiejsca]);
            } else if (komenda.equals("zwolnij")) {
                System.out.println("Podaj numer miejsca:");
                int numerMiejsca = scanner.nextInt();

                if (numerMiejsca >= 0 && numerMiejsca < 5) {
                    if(miejscaParkingowe[numerMiejsca]) {
                        miejscaParkingowe[numerMiejsca] = false;
                        System.out.println("Miejsce zostało zwolnione!");
                    }else{
                        System.out.println("Miejsce było wolne");
                    }
                } else {
                    System.out.println("Niepoprawny numer miejsca!");
                }

            } else if (komenda.equals("wypisz")) {
                for (int i = 0; i < miejscaParkingowe.length; i++) {
                    System.out.print("Zajęte[" + i + "]: " + miejscaParkingowe[i] + ", ");
                }
                System.out.println();
            }

        } while (!komenda.equals("exit"));
    }
}
