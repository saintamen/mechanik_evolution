package com.sda;

import java.util.Scanner;

public class MainTablicaMechanikObiektKompozycja {
    public static void main(String[] args) {
        Mechanik mechanik = new Mechanik("Marianex", 10);

        Scanner scanner = new Scanner(System.in);
        System.out.println();

        String komenda;
        do { // wykona się co najmniej raz
            System.out.println("Podaj komendę: (dodaj/sprawdz/wypisz)");
            komenda = scanner.next();

            if (komenda.equals("dodaj")) {
                System.out.println("Podaj numer miejsca:");
                int numerMiejsca = scanner.nextInt();

                mechanik.dodaj(numerMiejsca);

            } else if (komenda.equals("sprawdz")) {
                System.out.println("Podaj numer miejsca:");
                int numerMiejsca = scanner.nextInt();

                mechanik.sprawdzMiejsce(numerMiejsca);

            } else if (komenda.equals("zwolnij")) {
                System.out.println("Podaj numer miejsca:");
                int numerMiejsca = scanner.nextInt();

                mechanik.zwolnij(numerMiejsca);

            } else if (komenda.equals("wypisz")) {
                mechanik.wypiszStanMiejsc();
            }

        } while (!komenda.equals("exit"));

    }
}
