package com.sda;

public class MiejsceParkingowe {
    private int numerMiejsca;
    private boolean czyZajete;

    public MiejsceParkingowe(int numerMiejsca) {
        this.numerMiejsca = numerMiejsca;
        this.czyZajete = false;
    }

//    public MiejsceParkingowe(int numerMiejsca, boolean czyZajete) {
//        this.numerMiejsca = numerMiejsca;
//        this.czyZajete = czyZajete;
//    }
    public boolean czyZajete(){
        return czyZajete;
    }

    public void sprawdzZajetosc(){
        if(czyZajete){
            System.out.println("Miejsce zajete");
        }else{
            System.out.println("Miejsce wolne");
        }
    }

    public void zajmijMiejsce(){
        if(czyZajete){ // jeśli zajęte
            System.out.println("Miejsce juz bylo zajete.");
        }else{ // jeśli nie zajęte
            czyZajete = true;
            System.out.println("Zajmuje miejsce");
        }
    }

    public void zwolnijMiejsce(){
        if(czyZajete){
            System.out.println("Zwolniles miejsce");
            czyZajete = false;
        }else{
            System.out.println("Miejsce bylo wolne!");
        }
    }
}
