package com.sda;

public class Main {
    public static void main(String[] args) {
        // pętla
        // rozpoczyna od -500
        // wykonuje się 1200 razy
        // skacze co 2 element
        for (int i = -500; i < 700; i += 2) {
            System.out.print(i);
        }

        // i=+2 // i=2 <- podstawienie
        // i+=2 // i=i+2 <- przeskok o 2

        // rozpoczyna od 300
        // wykonuje się 1000 razy
        // skok powinien być wykładniczy (mnożymy razy 2)
        int j = 300;
        int licznik = 0;
        while (licznik < 1000){
            j *=2;

            licznik++;
        }

        // rozpoczyna się od 3
        // wykonuje się dopóki iterator nie będzie miał wartości 1000
        // skok wykładniczo, mnożąc razy 3
        for (int i = 3; i < 1000; i=i*3) {
            System.out.println(i);
        }

        // rozpoczyna się od 8
        // wykonuje się dopóki iterator nie będzie miał wartości 5000
        // skok liniowy (+1)
        // wypisz liczby podzielne przez 7 i 8
        for (int i = 8; i < 5000; i++) {
            if(i%7==0 && i%8==0){
                System.out.println(i);
            }
        }
    }
}
