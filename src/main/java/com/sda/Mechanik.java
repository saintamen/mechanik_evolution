package com.sda;

public class Mechanik {
    private String nazwa;
    private MiejsceParkingowe[] miejsca;

    public Mechanik(String nazwa, int iloscMiejsc) {
        this.nazwa = nazwa;
        this.miejsca = new MiejsceParkingowe[iloscMiejsc];
        for (int i = 0; i < miejsca.length; i++) {
            miejsca[i] = new MiejsceParkingowe(i);
        }
    }

    public void dodaj(int numerMiejsca){
        if (numerMiejsca >= 0 && numerMiejsca < miejsca.length) {
            miejsca[numerMiejsca].zajmijMiejsce();
        } else {
            System.out.println("Niepoprawny numer miejsca!");
        }
    }

    public void sprawdzMiejsce(int numerMiejsca){
        miejsca[numerMiejsca].sprawdzZajetosc();
    }

    public void zwolnij(int numerMiejsca){
        if (numerMiejsca >= 0 && numerMiejsca < miejsca.length) {
            miejsca[numerMiejsca].zwolnijMiejsce();
        } else {
            System.out.println("Niepoprawny numer miejsca!");
        }
    }

    public void wypiszStanMiejsc(){
        for (int i = 0; i < miejsca.length; i++) {
            miejsca[i].sprawdzZajetosc();
        }
    }
}
