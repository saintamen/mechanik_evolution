package com.sda;

import java.util.Scanner;

public class MainTablicaMechanikObiekt {

    public static void main(String[] args) {
        MiejsceParkingowe[] miejsca = new MiejsceParkingowe[5];
        for (int i = 0; i < miejsca.length; i++) {
            miejsca[i] = new MiejsceParkingowe(i);
        }

        Scanner scanner = new Scanner(System.in);
        System.out.println();

        String komenda;
        do { // wykona się co najmniej raz
            System.out.println("Podaj komendę: (dodaj/sprawdz/wypisz)");
            komenda = scanner.next();

            if (komenda.equals("dodaj")) {
                System.out.println("Podaj numer miejsca:");
                int numerMiejsca = scanner.nextInt();

                if (numerMiejsca >= 0 && numerMiejsca < 5) {
                    miejsca[numerMiejsca].zajmijMiejsce();
                } else {
                    System.out.println("Niepoprawny numer miejsca!");
                }
            } else if (komenda.equals("sprawdz")) {
                System.out.println("Podaj numer miejsca:");
                int numerMiejsca = scanner.nextInt();

                miejsca[numerMiejsca].sprawdzZajetosc();

            } else if (komenda.equals("zwolnij")) {
                System.out.println("Podaj numer miejsca:");
                int numerMiejsca = scanner.nextInt();

                if (numerMiejsca >= 0 && numerMiejsca < 5) {
                    miejsca[numerMiejsca].zwolnijMiejsce();
                } else {
                    System.out.println("Niepoprawny numer miejsca!");
                }

            } else if (komenda.equals("wypisz")) {
                for (int i = 0; i < miejsca.length; i++) {
                    miejsca[i].sprawdzZajetosc();
                }
            }

        } while (!komenda.equals("exit"));
    }
}
